#pragma once

#include "Algorithms/stdafx.h"
#include "Util.h"

// Implement rule_of_three

class rule_of_three
{
	char* m_data;
	size_t m_size;

public:
	rule_of_three(char* data = nullptr)
		: m_data(data)
		, m_size(0)
	{
		if (data)
		{
			const size_t sz = strlen(data);
			m_data = new char[sz + 1]{ '\0' };
			strcpy(m_data, data);
			m_size = sz;
		}
	}

	rule_of_three(const rule_of_three& other)
		: m_data(other.m_data)
		, m_size(0)
	{
		if (other.m_data)
		{
			m_data = new char[other.m_size + 1]{ '\0' };
			strcpy(m_data, other.m_data);
			m_size = other.m_size;
		}
	}

	rule_of_three& operator=(const rule_of_three& other)
	{
		if (other.m_size > m_size)
		{
			rule_of_three temp(other);
			std::swap(temp.m_data, m_data);
			m_size = temp.m_size;
		}
		else
		{
			strcpy(m_data, other.m_data);
			m_data[other.m_size] = '\0';
			m_size = other.m_size;
		}
		return *this;
	}

	~rule_of_three()
	{
		delete[] m_data;
	}

	friend std::ostream& operator<<(std::ostream& os, rule_of_three const& r)
	{
		if (r.m_data)
			os << r.m_data;
		return os;
	}
};




void test_rule_of_three()
{
	using namespace Util;
	std::ostream& os = std::cout;
	{
		test(os, "Executing rule_of_three",
			[]() {
			rule_of_three r1 = "jagan";
			rule_of_three r2 = "sai";
			std::cout << "Before assignment...\n" << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_three",
			[]() {
			rule_of_three r1 = "jagan";
			rule_of_three r2 = r1;
			std::cout << "Testing copy construction...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_three",
			[]() {
			rule_of_three r1 = "jagan";
			rule_of_three r2 = "gagan";
			std::cout << "Before assignment..." << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
}


