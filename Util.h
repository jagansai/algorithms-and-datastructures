#pragma once
#include "Algorithms/stdafx.h"


#define MAKE_MAKE_UNIQUE_FRIEND(TYPE) \
template<typename TYPE, typename... Args> \
friend std::unique_ptr<TYPE> std::make_unique<TYPE>(Args&&...)

namespace Util
{
	template<typename Func>
	inline void test(std::ostream& os, std::string msg, Func&& func)
	{
		os << "Executing..." << msg << '\n';
		func();
		os << "Completed executing..." << msg << '\n';
	}

	template<typename Container>
	struct ReverseWrapper
	{
		Container& cont_;

		ReverseWrapper(Container& cont)
			: cont_(cont)
		{
		}

		auto begin() -> decltype(std::rbegin(cont_))
		{
			return std::rbegin(cont_);
		}

		auto end() -> decltype(std::rend(cont_))
		{
			return std::rend(cont_);
		}
	};


	template<typename Container>
	auto reverse(Container& cont)
	{
		return Util::ReverseWrapper<Container>(cont);
	};

	template<typename Type>
	class Singleton
	{
	public:
		Singleton() {}
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;
		Singleton(Singleton&&) = delete;
		Singleton& operator=(Singleton&&) = delete;

	public:
		// using call_once. 
		static Type& makeInstance()
		{
			std::call_once(Singleton::flag_, [&] {
				instance_ = Type::instance();
			});
			return *Singleton::instance_;
		}
		// we could also static initializer
		static Type& makeInstance_static()
		{
			static std::unique_ptr<Type> instance_local = Type::instance();
			return *instance_local;
		}


	private:
		static std::unique_ptr<Type> instance_;
		static std::once_flag flag_;
	};

	template<typename Type>
	std::unique_ptr<Type>  Singleton<Type>::instance_ = nullptr;

	template<typename Type>
	std::once_flag Singleton<Type>::flag_;
}


namespace Util
{
	namespace Time
	{
		constexpr auto ms(const size_t utime)
		{
			return std::chrono::milliseconds(utime);
		}

		constexpr auto sec(const size_t utime)
		{
			return std::chrono::seconds(utime);
		}

		constexpr auto micros(const size_t utime)
		{
			return std::chrono::microseconds(utime);
		}
	}
}


namespace Util
{
	namespace Algorithms
	{
		template<typename FwdIter, typename Type>
		auto getAllElementsImpl(FwdIter first, FwdIter last, Type const& to_find, std::random_access_iterator_tag)
		{
			std::cout << "random iterator\n";
			auto lower = std::lower_bound(first, last, to_find);
			auto upper = std::upper_bound(lower, last, to_find);
			return std::vector<Type>{lower, upper};
		}

		template<typename FwdIter, typename Type>
		auto getAllElementsImpl(FwdIter first, FwdIter last, Type const& to_find, std::bidirectional_iterator_tag)
		{
			std::cout << "bi-directional iterator\n";
			std::vector<Type> result;
			result.reserve(20); // some magic number.

			for (; first != last; ++first)
			{
				if (*first == to_find)
				{
					result.emplace_back(to_find);
				}
			}
			return result;
		}

		template<typename FwdIter, typename Type>
		auto getAllElements(FwdIter first, FwdIter last, Type const& to_find)
		{
			return getAllElementsImpl(first, last, to_find, std::iterator_traits<FwdIter>::iterator_category());
		}


		bool FindIfSquare()
		{
			int m = 16;
			bool isperfect = false;
			long sqroot = 0;
			if (m >= 0)
			{
				sqroot = (long)(::sqrt(m) + 0.5);
				isperfect = ((sqroot * sqroot) == m);
			}

			if (isperfect)
			{
				std::cout << m << " is a square, root is " << sqroot << '\n';
			}
			return isperfect;

		}

		template<typename FwdIter, typename Func>
		void adjacent_pair(FwdIter first, FwdIter last, Func f)
		{
			if (first != last)
			{
				FwdIter trailer = first;
				++first;

				for (; first != last; ++first, ++trailer)
				{
					f(*trailer, *first);
				}
			}
		}

		template<typename FwdIter, typename Func>
		void for_all_pairs(FwdIter first, FwdIter last, Func f)
		{
			if (first != last)
			{
				FwdIter trailer = first;
				++first;

				for (; first != last; ++first, ++trailer)
				{
					for (FwdIter it = first; it != last; ++it)
					{
						f(*trailer, *it);
					}
				}
			}
		}
	}
}
