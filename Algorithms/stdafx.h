// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#ifndef STDAFX_H
#define STDAFX_H

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
#include <random>
#include <fstream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <future>
#include <queue>
#include <chrono>
#include <memory>


#endif


// TODO: reference additional headers your program requires here
