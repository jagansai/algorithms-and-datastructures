#pragma once
#include "stdafx.h"
#include "RefCounter.h"



struct Deleter
{
	template<typename T>
	void operator()(const T* ptr) const
	{
		std::cout << "Deleting " << *ptr << '\n';
		delete ptr;
	}
};

template<typename T, typename deleter = Deleter>
class RefCountedSafePtr
{
	RefCounter *m_refCounter;
	T* m_ptr;

public:
	explicit RefCountedSafePtr(T* ptr = nullptr)
		: m_ptr(nullptr)
	{
		if (ptr)
		{
			m_ptr = ptr;
			m_refCounter = new RefCounter();
		}
	}

	RefCountedSafePtr(const RefCountedSafePtr& other)
	{
		if (other.m_ptr)
		{
			m_ptr = other.m_ptr;
			m_refCounter = other.m_refCounter;
		}
	}

	RefCountedSafePtr& operator=(const RefCountedSafePtr& other)
	{
		if (this != &other)
		{
			if (m_refCounter)
			{
				m_refCounter->decrement();
				m_ptr = other.m_ptr;
				m_refCounter = other.m_refCounter;
			}
		}
		return *this;
	}

	T* release()
	{
		T* ptr = m_ptr;
		m_refCounter->decrement();
		m_ptr = nullptr;
		return ptr;
	}

	T* get()
	{
		return m_ptr;
	}

	T& operator*()
	{
		return *m_ptr;
	}

	~RefCountedSafePtr()
	{
		if (m_refCounter->refCount() == 0)
		{
			deleter()(m_ptr);
		}
		else
		{
			m_refCounter->decrement();
		}
	}

};