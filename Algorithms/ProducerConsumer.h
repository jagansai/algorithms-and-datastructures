#ifndef PRODUCERCONSUMER_H
#define PRODUCERCONSUMER_H

#include "stdafx.h"
#include "MyUtility.h"

enum ConsumersType
{
	Single = 1,
	Multiple
};


template<typename T>
class Queue
{

private:
	typedef std::unique_lock<std::mutex> Lock;
	typedef std::queue<T> Container;

	// member declarations
	std::mutex m_mutex;
	std::condition_variable m_push_wait_cv;
	std::condition_variable m_pop_wait_cv;
	const size_t ulimit = 100;
	const size_t wait_limit = 50; // millisec
	Container m_container;

public:
	Queue() = default;
	Queue(const Queue&) = default;
	Queue& operator=(const Queue&) = default;
	Queue(Queue&&) = default;
	Queue& operator=(Queue&&) = default;
	

	bool push(const T& value)
	{
		Lock lg{ m_mutex };
		while (m_container.size() > ulimit)
		{
			if (!m_pop_wait_cv.wait_for(lg, Util::Time::ms(wait_limit), [&]() { return m_container.size() < ulimit; }))
			{
				return false;
			}
		}
		m_container.push(value);
		m_push_wait_cv.notify_all();
		return true;
	}

	std::pair<bool, T> pop()
	{
		Lock lg{ m_mutex };
		while (m_container.empty())
		{
			if (!m_push_wait_cv.wait_for(lg, Util::Time::ms(wait_limit), [&]() { return !m_container.empty(); }))
			{
				return std::make_pair(false, T{});
			}
		}
		auto p = std::make_pair(true, m_container.front());
		m_container.pop();
		m_pop_wait_cv.notify_all();
		return p;
	}
};




class ProducerConsumer
{
private:
	Queue<int> m_queue;
	std::ostream& m_os;
	static const int max_elements = 1000;
	static const int sleep_for = 50; // milliseconds;
	bool done = false;
	static const int max_retry_count = 5;

public:
	ProducerConsumer(std::ostream& os) : m_queue{}, m_os(os) {}
	void produce()
	{
		int count = 1;
		while (count < max_elements)
		{
			bool success = m_queue.push(count);
			if (!success)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(ProducerConsumer::sleep_for));
			}
			else
			{
				++count;
			}
		}
		done = true;
	}

	void consume()
	{
		int retry_count = 0;
		while (true)
		{
			auto result = m_queue.pop();
			if (result.first == false)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(ProducerConsumer::sleep_for));
				if (done)
				{
					++retry_count;
				}
				if (doneProcessingAllItems(retry_count))
				{
					break;
				}
			}
			else
			{
				m_os << result.second << "\n";
			}
		}
	}

private:
	bool doneProcessingAllItems(const int retry_count) const
	{
		return retry_count >= max_retry_count && ProducerConsumer::done;
	}
};



void ProducerConsumerTest()
{
	std::ofstream outfile("outfile.txt");
	ProducerConsumer p{ outfile };
	std::thread producer([&] { p.produce(); });
	
	std::vector<std::future<void>> consumers;

	for (int i = 0; i < 5; ++i)
	{
		consumers.push_back(std::async([&] { p.consume(); }));
	}


	for (auto&& t : consumers)
	{
		t.get();
	}
	producer.join();

	std::cout << "Processed all the items... Exiting\n";
}




#endif