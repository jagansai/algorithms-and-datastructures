#pragma once

class RefCounter
{
	int m_counter;

public:
	RefCounter()
		: m_counter(1) 
	{
	}

	RefCounter(const RefCounter& other)
	{
		m_counter = other.m_counter + 1;
	}

	RefCounter& operator=(const RefCounter& other)
	{
		if (this != &other)
		{
			m_counter = other.m_counter;
		}
	}

	void increment()
	{
		++m_counter;
	}

	void decrement()
	{
		--m_counter;
	}

	int refCount() const
	{
		return m_counter;
	}

	~RefCounter()
	{
		--m_counter;
	}

};