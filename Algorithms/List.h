#pragma once
#include "stdafx.h"


/*/
**  This is Single Linked List implementation.
*/


template<typename Type>
class List
{

	// implement a node internally as inner class.
	struct Node
	{
		Type m_data;
		std::unique_ptr<Node> m_next;

		Node() = default;
		Node(Type const& data) : m_data(data) {}
		Node(Type&& data) : m_data(std::move(data)) {}
	};

private:
	std::unique_ptr<Node> m_head;

public:

	List(Type const& data) : m_head(std::make_unique<Node>(data)) {}
	
	// Default declarations.
	List() = default;
	List(List<Type> const& other)
	{
		
	}
	List& operator=(List const&) = default;
	List(List&&) = default;
	List& operator=(List&&) = default;

public:
	void push_back(Type const& data)
	{
		if (!m_head)
		{
			m_head = std::make_unique<Node>(data);
		}
		else
		{
			Node* temp = m_head.get();
			while (temp->m_next) temp = temp->m_next.get();
			temp->m_next = std::make_unique<Node>(data);
		}
	}



public:
	friend std::ostream& operator<<(std::ostream& os, List const& other)
	{
		Node* temp = other.m_head.get();
		while (temp)
		{
			os << temp->m_data << '\n';
			temp = temp->m_next.get();
		}
		return os;
	}


private:

	typedef Node InternalNode;
	// Iterator implementation.

	struct Iterator
	{
		Type* m_data;
	public:
		Iterator(Type* data) : m_data(data) 
		{
		}
		
		Iterator() : m_data(nullptr)
		{
		}

		Iterator(const Iterator& other) : m_data(other.m_data)
		{
		}

		Iterator& operator=(const Iterator& other)
		{
			m_data = other.m_data;
			return *this;
		}

		Iterator(Iterator&& other)
		{
			m_data = other.m_data;
			m_data = nullptr;
		}

		Iterator& operator=(Iterator&& other)
		{
			m_data = other.m_data;
			other.m_data = nullptr;
		}

		Iterator operator++(int)
		{
			Iterator temp(*this);
			m_data = m_data->m_next.get();
			return temp;
		}

		Iterator& operator++()
		{
			m_data = m_data->m_next.get();
			return *this;
		}

		bool operator!=(const Iterator& other) const
		{
			return m_data != other.m_data;
		}	

		Type operator*()
		{
			return *m_data;
		}

	};


	public:
		Iterator begin()
		{
			return Iterator(m_head.get());
		}

		Iterator end()
		{
			return Iterator();
		}
};


inline void ListTest()
{
	{
		List<int> l;
		l.push_back(10);
		l.push_back(20);
		l.push_back(30);
		l.push_back(40);
		l.push_back(50);

		std::cout << l << '\n';
	}
	{
		List<int> l1;
		l1.push_back(10);
		l1.push_back(20);
		l1.push_back(30);
		List<int> l2 = l1;
	}
}


