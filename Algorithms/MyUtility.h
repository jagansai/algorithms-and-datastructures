#ifndef MYUTILITY_H
#define MYUTILITY_H

#include <type_traits>
#include <chrono>
#include <utility>

typedef std::chrono::duration<std::chrono::milliseconds> ms;
typedef std::chrono::duration<std::chrono::seconds> secs;
typedef std::chrono::high_resolution_clock Clock_t;
typedef std::chrono::time_point<std::chrono::steady_clock> TimePoint;

namespace CPPUtils
{
    struct Timer
    {
        const std::string m_tag;
        TimePoint m_start;
        std::ostream& m_os;

        Timer(const std::string& tag, std::ostream& os)
            : m_tag(tag)
			, m_start(Clock_t::now())
            , m_os(os)
            {
            }

        ~Timer()
        {
            auto finish = Clock_t::now();
            double elapsed = (std::chrono::duration_cast<std::chrono::milliseconds>(finish - m_start).count() * 1000.0)/1000.0;

            if (elapsed > 1000.0)
            {
                m_os << m_tag << ":" << elapsed / 1000.0 << " secs.";
            }
            else
            {
                m_os << m_tag << ":" << elapsed << " ms.";
            }
        }


        private:
        Timer(const Timer&);
        Timer& operator=(const Timer&);
        Timer(Timer&&);
        Timer& operator=(Timer&&);
    };


    template<typename Func>
    auto TimeTaken(Func&& f, const char* tag, std::ostream& os)
        -> decltype(f())
    {
        Timer t(tag, os);
        return f();
    }

    template<typename Func, typename... Args>
    auto TimeTaken(Func&& f, const char* tag, std::ostream& os, Args&&... args)
        -> decltype(f(std::forward<Args>(args)...))
    {
        Timer t(tag, os);
        return f(std::forward<Args>(args)...);
    }



	// Check for the types passed to the template are all of same type.

	template<typename T, typename ...TS>
	struct AllSameTypes
	{
	};

	template<typename T>
	struct AllSameTypes<T>
	{
		static constexpr bool value = true;
	};

	template<typename T1, typename T2>
	struct AllSameTypes<T1, T2>
	{
		static constexpr bool value = std::is_same<T1, T2>::value;
	};

	template<typename T1, typename T2, typename ...TS>
	struct AllSameTypes<T1, T2, TS...>
	{
		static constexpr bool value = std::is_same<T1, T2>::value ? AllSameTypes<T2, TS...>::value : false;
	};

}

#endif