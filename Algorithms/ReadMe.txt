========================================================================
    CONSOLE APPLICATION : Algorithms Project Overview
========================================================================

Algorithms.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

Algorithms.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

Algorithms.cpp
    This is the main application source file.

Other Header Files
    The algorithms are implemented in other header files. Each header file is included in the Algorithms.cpp file.


/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named Algorithms.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////

Tested: The algorithms are tested on Visual Studio 2015.

TODO: Introduce Unit testing framework to test the code.
TODO: Introduce build script for compiling on UNIX variant systems as well.


/////////////////////////////////////////////////////////////////////////////
