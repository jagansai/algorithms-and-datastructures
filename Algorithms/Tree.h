#ifndef TREE_H
#define TREE_H

#include "stdafx.h"
#include <memory>


template<typename Type, typename Compare = std::less<Type>>
class Tree
{

private:
	struct Node
	{
		Type m_data;
		typedef std::unique_ptr<Node> NodePtr;


		NodePtr m_left;
		NodePtr m_right;
		Node* m_sibling;

		Node(Type const& data)
			: m_data(data)
			, m_sibling(nullptr)
		{
		}

		static auto make_node(Type const& data)
		{
			return std::make_unique<Node>(data);
		}
	};

private:
	std::unique_ptr<Node> m_head;
	Compare m_compare;

	typedef std::unique_ptr<Node> node_ptr;
	typedef const std::unique_ptr<Node> const_node_ptr;

public:
	Tree(Type const& data, Compare compare)
		: m_head(data)
		, m_compare(compare)
	{
	}

	Tree()
		: m_compare()
	{
	}

	explicit Tree(Compare compare)
		: m_compare(compare)
	{
	}

	Tree(Tree&& other)
		: m_head(std::move(other.m_head))
		, m_compare(other.m_compare)
	{
	}

	Tree& operator=(Tree&& other)
	{
		if (this != &other)
		{
			m_head = std::move(other.m_head);
			m_compare = other.m_head;
		}
		return *this;
	}


	Tree(Tree const& other)
		: m_compare(other.m_compare)
	{
		Clone(other);
	}

	Tree& operator=(const Tree& other)
	{
		if (this != &other)
		{
			m_compare = other.m_compare;
			Clone(other);
		}
		return *this;
	}


	void Add(Type const& data)
	{
		if (!m_head)
		{
			m_head = std::make_unique<Node>(data);
		}
		else
		{
			Insert(m_head, data);
		}
	}

	void Print(std::ostream& os = std::cout)
	{
		Print(m_head, os);
	}


private:
	void Insert(node_ptr& node, Type const& data)
	{
		if (!node)
		{
			node = std::make_unique<Node>(data);
			return;
		}
		if (m_compare(data, node->m_data))
		{
			Insert(node->m_left, data);
		}
		else
		{
			Insert(node->m_right, data);
		}
	}

	void Clone(Tree const& other)
	{
		if (!other.m_head)
			return;

		m_head = Node::make_node(other.m_head->m_data);
		Clone(m_head, other.m_head);
	}

	void Clone(node_ptr& node, const_node_ptr& otherNode)
	{
		if (!otherNode)
			return;
		node.swap(Node::make_node(otherNode->m_data));
		Clone(node->m_left, otherNode->m_left);
		Clone(node->m_right, otherNode->m_right);
	}


	void Print(const_node_ptr& node, std::ostream& os)
	{
		if (node)
		{
			Print(node->m_left, os);
			os << node->m_data << '\n';
			Print(node->m_right, os);
		}
	}

	void preorder(const node_ptr& root) {
		if (root) {
			std::cout << root->m_data << ' ';
			preorder(root->m_left);
			preorder(root->m_right);
		}
	}

	void posorder(const node_ptr& root) {
		if (root) {
			posorder(root->left);
			posorder(root->right);
			std::cout << root->value << ' ';
		}
	}

};
//
//	node_ptr minimum(node_ptr& root) {
//		if (!root || !root->left)
//			return std::move(root);
//
//		return std::move(minimum(root->left));
//	};
//
//	node_ptr remove(node_ptr& root, const T& value) {
//		if (!root)
//			return std::move(root);
//
//		if (value > root->value)
//			root->right = std::move(remove(root->right, value));
//		else if (value < root->value)
//			root->left = std::move(remove(root->left, value));
//		else {
//			node_ptr here = std::move(root);
//			if (here->left == here->right)
//				return node_ptr();
//
//			if (!here->right)
//				return std::move(here->left);
//
//			if (!here->left)
//				return std::move(here->right);
//
//			node_ptr min = std::move(minimum(here->right));
//			min->left = std::move(here->left);
//			if (here->right)
//				min->right = std::move(remove(here, value));
//			return std::move(min);
//		}
//		return std::move(root);
//	}
//
//public:
//	binary_tree() {
//	}
//
//	void insert(const T& value) {
//		insert(root_, value);
//	}
//
//	void order() {
//		order(root_);
//		cout << endl;
//	}
//	void preorder() {
//		preorder(root_);
//		cout << endl;
//	}
//	void posorder() {
//		posorder(root_);
//		cout << endl;
//	}
//
//	void remove(const T& value) {
//		root_ = std::move(remove(root_, value));
//	}
//
//	bool search(const T& value) {
//		return search(root_, value);
//	}
//};



struct Employee
{
	double salary;
	std::string name;

	Employee(std::string name, double salary)
		: name(std::move(name))
		, salary(salary)
	{

	}

	Employee(Employee&&) = default;
	Employee(Employee const&) = default;
	Employee& operator=(Employee&&) = default;
	Employee& operator=(Employee const&) = default;


	static bool ByName(Employee const& lhs, Employee const& rhs)
	{
		return lhs.name < rhs.name;
	}

	static bool BySal(Employee const& lhs, Employee const& rhs)
	{
		return lhs.salary < rhs.salary;
	}


	friend std::ostream& operator<<(std::ostream& os, Employee const& other)
	{
		os << other.name << "," << other.salary;
		return os;
	}

};

void TreeDemo()
{
	{
		Tree<int> tree;
		tree.Add(5);
		tree.Add(3);
		tree.Add(4);
		tree.Add(2);
		tree.Add(2);
		tree.Add(6);
		tree.Add(7);
		std::cout << "Printing contents of the tree\n";
		tree.Print();

	}
	{
		typedef bool(*Comparator)(Employee const&, Employee const&);

		Tree<Employee, Comparator> tree(Employee::BySal);
		tree.Add(Employee{ "Jagan", 105 });
		tree.Add(Employee{ "Nath", 103 });
		tree.Add(Employee{ "Pantula", 104 });
		tree.Add(Employee{ "Sai", 107 });

		std::cout << "Printing contents of the tree\n";
		tree.Print();
	}

	{
		typedef bool(*Comparator)(Employee const&, Employee const&);

		Tree<Employee, Comparator> tree(Employee::ByName);
		tree.Add(Employee{ "Jagan", 105 });
		tree.Add(Employee{ "Nath", 103 });
		tree.Add(Employee{ "Pantula", 104 });
		tree.Add(Employee{ "Sai", 107 });

		std::cout << "Printing contents of the tree\n";
		tree.Print();
	}
	{
		Tree<int> tree;
		tree.Add(5);
		tree.Add(4);
		tree.Add(3);
		tree.Add(7);
		tree.Add(6);

		Tree<int> anotherTree = tree;
		std::cout << "Copy constructing a tree from other tree\n";
		anotherTree.Print();
	}
	{
		{

			std::cout << "Demo of copy assignment of a tree\n";
			Tree<int> tree;
			tree.Add(5);
			tree.Add(4);
			tree.Add(3);
			tree.Add(7);
			tree.Add(6);

			std::cout << "Printing contents of tree\n";
			tree.Print();

			Tree<int> anotherTree;
			anotherTree.Add(10);
			anotherTree.Add(8);
			anotherTree.Add(11);

			std::cout << "Printing contents of tree\n";
			anotherTree.Print();

			std::cout << "Assigning the first tree to second.\n";

			anotherTree = tree;
			anotherTree.Print();
		}
	}
}

#endif