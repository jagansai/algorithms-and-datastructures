
#ifndef GETUNIQUEITEMS_H
#define GETUNIQUEITEMS_H

#include "stdafx.h"
#include "RemoveDuplicates.h"

template<typename Type>
std::vector<Type> GetUniqueItems(std::vector<Type> source)
{
	std::sort(source.begin(), source.end());

	std::vector<Type> result;
	result.reserve(source.size());

	auto start = source.begin();

	while (start != source.end())
	{
		auto duplicatePos = FindDuplicateRange(start, source.end());
		if (duplicatePos.first != source.end())
		{
			result.insert(result.end(), start, duplicatePos.first);
			start = duplicatePos.second;
		}
	}
	return result;
}



void GetUniqueItemsDemo()
{
	{
		std::vector<int> v{ 1,2,3,4,4 };
		Print(v, "Original data");
		Print(GetUniqueItems(v), "Unique items");
	}
}


#endif