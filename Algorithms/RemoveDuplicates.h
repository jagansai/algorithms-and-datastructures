
#ifndef REMOVEDUPLICATES_H
#define REMOVEDUPLICATES_H

#include "stdafx.h"
#include "MyUtility.h"


template<typename Iter>
std::pair<Iter, Iter> FindDuplicateRange(Iter start, Iter finish)
{
	auto upperbound = finish;

	auto duplicatePos = std::adjacent_find(start, finish);
	if (duplicatePos != finish)
	{
		upperbound = std::upper_bound(duplicatePos, finish, *duplicatePos);
	}
	return std::make_pair(duplicatePos, upperbound);
}


template<typename Type, typename Comparator = std::less<Type> >
void RemoveDuplicates(std::vector<Type>& v, Comparator cmp = Comparator())
{
	std::sort(v.begin(), v.end(), cmp);
	auto start = v.begin();
	while (start != v.end())
	{
		auto p = FindDuplicateRange(start, v.end());
		if (p.first != v.end())
		{
			start = v.erase(std::next(p.first), p.second);
		}
		else
		{
			break;
		}
	}
}

template<typename Type>
void Print(std::vector<Type> const& v, const char* msg, std::ostream& os = std::cout)
{
	std::cout << msg << '\n';
	for (auto const& e : v)
	{
		os << e << '\n';
	}
}

void RemoveDuplicatesTest()
{

	{
		std::vector<int> v{ 1,2,3,4,4,2,1,5 };
		Print(v, "Before removing duplicates");
		RemoveDuplicates(v);
		Print(v, "After removing duplicates");
	}

	{
		std::vector<int> v{ 1,2,3,4 };
		Print(v, "Before removing duplicates");
		RemoveDuplicates(v);
		Print(v, "After removing duplicates");
	}
	{
		const int max = 100000;
		std::random_device rd;
		std::uniform_int_distribution<int> dist_d(1, max);
		std::vector<int> v;
		v.reserve(max);

		for (int i = 0; i < max; ++i)
		{
			v.push_back(dist_d(rd));
		}

		std::ofstream outfile("outfile.txt", std::ios::binary | std::ios::out);
		Print(v, "Before removing duplicates", outfile);

		std::string msg = "Time taken to remove duplicates for " + std::to_string(max) + " elements";
		CPPUtils::TimeTaken([&]() { RemoveDuplicates(v); }, msg.c_str(), std::cout);
		Print(v, "After removing duplicates", outfile);
	}
}

#endif

