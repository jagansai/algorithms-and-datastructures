  
       **Implementing the most frequently asked algorithms in the interviews.**   


Algorithms.cpp
    This is the main application source file.

Other Header Files
    The algorithms are implemented in other header files. Each header file is included in the Algorithms.cpp file.


Tested: The algorithms are tested on Visual Studio 2015.

TODO: Introduce Unit testing framework to test the code.
TODO: Introduce build script for compiling on UNIX variant systems as well.


/////////////////////////////////////////////////////////////////////////////