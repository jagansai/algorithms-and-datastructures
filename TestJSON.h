#pragma once

#include <string>
#include "Algorithms\json.hpp"

using json = nlohmann::json;

const char* jsonstr = R"foo(
{
   "name" : "Kartikeya",
   "age"  : 37
}
	)foo";


inline void TestJson()
{
	auto body = json::parse(jsonstr);
	
	for (auto iter = body.begin(); iter != body.end(); ++iter)
	{
		std::cout << iter.key() << ':' << iter.value() << '\n';
	}
}