#pragma once
#include "Algorithms/stdafx.h"
#include "Util.h"


// Implement rule_of_zero.


class rule_of_zero
{
	typedef std::vector<char> Buffer;
	Buffer m_data;

public:
	rule_of_zero(char* data = nullptr)
	{
		if (data)
		{
			const size_t sz = strlen(data);
			std::copy(data, data + sz, std::back_inserter(m_data));
		}
	}

	friend std::ostream& operator<<(std::ostream& os, rule_of_zero const& r)
	{
		std::copy(r.m_data.begin(), r.m_data.end(), std::ostream_iterator<char>(std::cout));
		return os;
	}
};

void test_rule_of_zero()
{
	using namespace Util;
	std::ostream& os = std::cout;
	{
		test(os, "Executing rule_of_zero",
			[]() {
			rule_of_zero r1 = "jagan";
			rule_of_zero r2 = "sai";
			std::cout << "Before assignment...\n" << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_zero",
			[]() {
			rule_of_zero r1 = "jagan";
			rule_of_zero r2 = r1;
			std::cout << "Testing copy construction...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_zero",
			[]() {
			rule_of_zero r1 = "jagan";
			rule_of_zero r2 = "gagan";
			std::cout << "Before assignment..." << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_zero",
			[]() {
			rule_of_zero r1 = "jagan";
			rule_of_zero r2 = "gagan";
			std::cout << "Before moving data...\n" << r1 << '\n' << r2 << '\n';
			r2 = std::move(r1);
			std::cout << "After move assignment...\n";
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_zero",
			[]() {
			rule_of_zero r1 = "jagan";
			std::cout << "Testing move construction...\n" << r1 << '\n';
			rule_of_zero r2 = std::move(r1);
			std::cout << "After move construction...\n";
			std::cout << r2 << '\n';
		});
	}
}



