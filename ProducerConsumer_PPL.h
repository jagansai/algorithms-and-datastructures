#pragma once
#include "Algorithms/stdafx.h"
#include <ppl.h>
#include <concurrent_queue.h>


namespace Concurrent
{
	typedef Concurrency::concurrent_queue<int> Queue;
	class ProducerConsumer
	{
		Queue m_queue;
		const int ulimit = 50;
		const int sleep_time = 50; // msec.
		const int max_items_to_produce = 100;
		std::atomic<bool> done = false;
	public:
		void produce()
		{
			int item_count = 0;
			while (item_count <= max_items_to_produce)
			{
				bool result = push(item_count + 1);
				if (result)
					++item_count;
				else
					std::this_thread::sleep_for(Util::Time::ms(50));
			}
			done = true;
		}
		void consume()
		{
			while (!done || !m_queue.empty())
			{
				auto p = pop();
				if (p.first)
					std::cout << p.second << '\n';
				else
					std::this_thread::sleep_for(Util::Time::ms(50));
			}
		}
	private:
		bool push(const int data)
		{
			if (m_queue.unsafe_size() < ulimit)
			{
				m_queue.push(data);
				return true;
			}
			return false;
		}

		std::pair<bool, int> pop()
		{
			if (m_queue.unsafe_size() > 0)
			{
				int data;
				bool result = m_queue.try_pop(data);
				return{ result, data };
			}
			return{ false, 0 };
		}
	};
}


inline void test_producer_consumer()
{
	Concurrent::ProducerConsumer p;
	auto produce = std::async([&] { p.produce(); });
	auto consumer1 = std::async([&] {p.consume(); });
	auto consumer2 = std::async([&] {p.consume(); });
	auto consumer3 = std::async([&] {p.consume(); });
	auto consumer4 = std::async([&] {p.consume(); });
	produce.get();
	consumer1.get();
	consumer2.get();
	consumer3.get();
	consumer4.get();
}