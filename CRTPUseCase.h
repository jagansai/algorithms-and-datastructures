#pragma once
#include "Algorithms/stdafx.h"
#include "Util.h"


class MyStaticData : public Util::Singleton<MyStaticData>
{
	friend class Util::Singleton<MyStaticData>;
	

private:
	static std::unique_ptr<MyStaticData> instance()
	{
		return std::unique_ptr<MyStaticData>(new MyStaticData("static data..."));
	}

	MyStaticData(std::string info) : info_(std::move(info))
	{
		std::cout << "constructing... MyStaticData\n";
	}

private:
	std::string info_;

public:

	const std::string& data() const
	{
		return info_;
	}
};
