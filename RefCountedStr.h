#pragma once
#include "Algorithms\stdafx.h"
#include <cstring>
#include "Algorithms\RefCounter.h"



class RefCountedStr
{
private:
	std::unique_ptr<char[]> m_data;
	size_t m_size;
	RefCounter* m_refCounter;
public:
	explicit RefCountedStr(const char* data = nullptr)
		: m_data()
		, m_size(0)	
		, m_refCounter(nullptr)
	{
		if (data)
		{
			init(data);
			m_refCounter = new RefCounter();
		}
	}

	void init(const char* data)
	{
		const int sz = strlen(data);
		m_data = std::unique_ptr<char[]>(new char[sz] {'\0'});
		strcpy(m_data.get(), data);
		m_size = sz;		
	}

	char* data()
	{
		return m_data.get();
	}

	const char* data() const
	{
		return m_data.get();
	}

	friend std::ostream& operator<<(std::ostream& os, RefCountedStr const& str)
	{
		if (str.m_data)
		{
			os << str.m_data.get();
		}
		return os;		
	}


	RefCountedStr(RefCountedStr const& other)
		: m_data()
		, m_size(0)
	{
		if (other.m_data) 
		{
			init(other.m_data.get());
			m_refCounter = other.m_refCounter;
		}
	}

	RefCountedStr& operator=(RefCountedStr const& other)
	{
		if (this != &other)
		{
			cleanup();
			init(other.m_data.get());
			m_refCounter = other.m_refCounter;
		}
	}

	void cleanup()
	{
		if (m_refCounter)
		{
			m_refCounter->decrement();
			if (m_refCounter->refCount() <= 0)
			{
				m_data.reset(nullptr);
			}
		}
	}

	~RefCountedStr()
	{
		cleanup();
	}
};
