#pragma once
#include "Algorithms/stdafx.h"



#pragma once

#include "Algorithms/stdafx.h"
#include "Util.h"

// Implement rule_of_five

class rule_of_five
{
	char* m_data;
	size_t m_size;

public:
	rule_of_five(char* data = nullptr)
		: m_data(data)
		, m_size(0)
	{
		if (data)
		{
			const size_t sz = strlen(data);
			m_data = new char[sz + 1]{ '\0' };
			strcpy(m_data, data);
			m_size = sz;
		}
	}

	rule_of_five(const rule_of_five& other)
		: m_data(other.m_data)
		, m_size(0)
	{
		if (other.m_data)
		{
			m_data = new char[other.m_size + 1]{ '\0' };
			strcpy(m_data, other.m_data);
			m_size = other.m_size;
		}
	}

	rule_of_five& operator=(const rule_of_five& other)
	{
		if (other.m_size > m_size)
		{
			rule_of_five temp(other);
			std::swap(temp.m_data, m_data);
			m_size = temp.m_size;
		}
		else
		{
			strcpy(m_data, other.m_data);
			m_data[other.m_size] = '\0';
			m_size = other.m_size;
		}
		return *this;
	}

	rule_of_five(rule_of_five&& other)
		: m_data(other.m_data)
		, m_size(other.m_size)
	{
		std::cout << "Move construction\n";
		other.m_data = nullptr;
	}

	rule_of_five& operator=(rule_of_five&& other)
	{
		delete[] m_data;
		std::cout << "Move assignment\n";
		m_data = other.m_data;
		m_size = other.m_size;
		other.m_data = nullptr;
		return *this;
	}
	
	~rule_of_five()
	{
		delete[] m_data;
	}

	friend std::ostream& operator<<(std::ostream& os, rule_of_five const& r)
	{
		if (r.m_data)
			os << r.m_data;
		return os;
	}
};




void test_rule_of_five()
{
	using namespace Util;
	std::ostream& os = std::cout;
	{
		test(os, "Executing rule_of_five",
			[]() {
			rule_of_five r1 = "jagan";
			rule_of_five r2 = "sai";
			std::cout << "Before assignment...\n" << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_five",
			[]() {
			rule_of_five r1 = "jagan";
			rule_of_five r2 = r1;
			std::cout << "Testing copy construction...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_five",
			[]() {
			rule_of_five r1 = "jagan";
			rule_of_five r2 = "gagan";
			std::cout << "Before assignment...\n" << r1 << '\n' << r2 << '\n';
			r2 = r1;
			std::cout << "After assignment...\n";
			std::cout << r1 << '\n';
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_five",
			[]() {
			rule_of_five r1 = "jagan";
			rule_of_five r2 = "gagan";
			std::cout << "Before moving data...\n" << r1 << '\n' << r2 << '\n';
			r2 = std::move(r1);
			std::cout << "After move assignment...\n";
			std::cout << r2 << '\n';
		});
	}
	{
		test(os, "Executing rule_of_five",
			[]() {
			rule_of_five r1 = "jagan";
			std::cout << "Testing move construction...\n" << r1 << '\n';
			rule_of_five r2 = std::move(r1);
			std::cout << "After move construction...\n";
			std::cout << r2 << '\n';
		});
	}
}


